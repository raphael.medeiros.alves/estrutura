# -*- coding: utf-8 -*-

from datetime import datetime
from algoritmos_ordenacao import *

metodos = {
    1: 'insertion-sort',
    2: 'selection-sort',
    3: 'merge-sort',
    4: 'quick-sort',
    5: 'counting-sort',
    6: 'radix-sort',
    7: 'heap-sort'}

instancias = {
    1: 'couting_min.txt',
    2: 'couting.txt',
    3: 'num.1000.1.in',
    4: 'num.1000.2.in',
    5: 'num.1000.3.in',
    6: 'num.1000.4.in',
    7: 'num.10000.1.in',
    8: 'num.10000.2.in',
    9: 'num.10000.3.in',
    10: 'num.10000.4.in',
    11: 'num.100000.1.in',
    12: 'num.100000.2.in',
    13: 'num.100000.3.in',
    14: 'num.100000.4.in'
}

# Iniciando
print('\n##### Atividades de ordenação #####\n')

# Exibindo instâncias
print('Instâncias:\n')
for chave, valor in instancias.items():
    print('{} - {}'.format(chave, valor))

instancia = input('\nEscolha a instância desejada acima: ')

# Transformando o arquivo com a instância escolhida em vetor
vetor = list()
nome_arquivo = instancias.get(instancia)
with open('instancias_ordenacao/{}'.format(nome_arquivo)) as arquivo:
    for linha in arquivo.readlines():
        vetor.append(int(linha.strip()))
arquivo.close()

# Exibindo métodos
print('\nMétodos de ordenação:\n')
for chave, valor in metodos.items():
    print('{} - {}'.format(chave, valor))

metodo = input('\nEscolha o método: ')
antes = datetime.now()
if metodo == 1:
    insertion_sort(vetor, len(vetor))
elif metodo == 2:
    selection_sort(vetor, len(vetor))
elif metodo == 3:
    merge_sort(0, len(vetor), vetor)
elif metodo == 4:
    quick_sort(0, len(vetor) - 1, vetor)
elif metodo == 5:
    vetor = counting_sort(vetor)
elif metodo == 6:
    vetor = radix_sort(vetor)
elif metodo == 7:
    heap_sort(vetor)
depois = datetime.now()

print('\nTempo de execução do {}: {} segundos'.format(metodos.get(metodo),
                                                      (depois - antes).total_seconds()))

# Salvando arquivo com a instância ordenada
salvar_arquivo = input('\nDeseja salvar em arquivo a instância ordenada (1 - Sim, 2 - Não): ')
if salvar_arquivo == 1:
    vetor = ['{}\n'.format(elemento) for elemento in vetor]
    arquivo_ordenado = open(nome_arquivo, 'w')
    arquivo_ordenado.writelines(vetor)
    arquivo_ordenado.close()
