# -*- coding: utf-8 -*-

from datetime import datetime
from algoritmos_gulosos import *

algoritmos = {
    1: 'kruskal',
    2: 'PRIM',
    3: 'Dijkstra'}

instancias = {
    # 1: 'dij07.txt',
    # 2: 'dij09.txt',
    1: 'dij10.txt',
    2: 'dij20.txt',
    3: 'dij40.txt',
    4: 'dij50.txt'
}

# Iniciando
print('\n##### Algoritmos Gulosos #####\n')

# Exibindo instâncias
print('Instâncias:\n')
for chave, valor in instancias.items():
    print('{} - {}'.format(chave, valor))

instancia = input('\nEscolha a instância desejada acima: ')

# Transformando o arquivo com a instância escolhida na matriz de adjacências
matriz = []
numero_vertices = 0
nome_arquivo = instancias.get(instancia)
with open('instancias_gulosos/{}'.format(nome_arquivo)) as arquivo:
    i = 0
    for linha in arquivo.readlines():
        numeros = linha.split(' ')
        if len(numeros) == 1 and i == 0:
            numero_vertices = int(numeros[0].strip())
            for n in range(numero_vertices):
                matriz.append([0] * numero_vertices)
        else:
            j = i + 1
            for numero in numeros:
                numero = numero.strip()
                if numero:
                    matriz[i][j] = int(numero)
                    matriz[j][i] = int(numero)
                    j += 1
            j = 0
            i += 1
arquivo.close()

# for linha in matriz:
#     print linha

# Exibindo métodos
print('\nAlgoritmos:\n')
for chave, valor in algoritmos.items():
    print('{} - {}'.format(chave, valor))

algoritmo = input('\nEscolha o algoritmo: ')
antes = datetime.now()
if algoritmo == 1:
    kruskal(numero_vertices, matriz)
elif algoritmo == 2:
    prim(numero_vertices, matriz)
elif algoritmo == 3:
    dijkstra(numero_vertices, matriz)
depois = datetime.now()

print('\nTempo de execução do {}: {} segundos'.format(algoritmos.get(algoritmo),
                                                      (depois - antes).total_seconds()))
