# -*- coding: utf-8 -*-

def selection_sort(vetor, tamanho_vetor):
    for i in range(0, tamanho_vetor):
        minimo = i
        for j in range(i + 1, tamanho_vetor):
            if vetor[j] < vetor[minimo]:
                minimo = j
        if vetor[i] != vetor[minimo]:
            aux = vetor[i]
            vetor[i] = vetor[minimo]
            vetor[minimo] = aux
        # print vetor

def insertion_sort(vetor, tamanho_vetor):
    pivo = None
    for i in range(1, tamanho_vetor):
        pivo = vetor[i]
        j = i - 1
        while j >= 0 and vetor[j] > pivo:
            vetor[j + 1] = vetor[j]
            j = j - 1
        vetor[j + 1] = pivo
        # print vetor

def merge_sort(inicio, fim, vetor):

    def merge(inicio, meio, fim, vetor):
        vetor_aux = [None for i in range(fim - inicio)]
        i = inicio
        j = meio
        k = 0

        while i < meio and j < fim:
            if vetor[i] <= vetor[j]:
                vetor_aux[k] = vetor[i]
                i += 1
            else:
                vetor_aux[k] = vetor[j]
                j += 1
            k += 1

        while i < meio:
            vetor_aux[k] = vetor[i]
            k += 1
            i += 1

        while j < fim:
            vetor_aux[k] = vetor[j]
            k += 1
            j += 1

        for i in range(inicio, fim):
            vetor[i] = vetor_aux[i - inicio]

    if inicio < fim - 1:
        meio = (inicio + fim) / 2
        merge_sort(inicio, meio, vetor)
        merge_sort(meio, fim, vetor)
        merge(inicio, meio, fim, vetor)
    # print vetor

def quick_sort(inicio, fim, vetor):

    def partition(inicio, fim, vetor):
        pivo = vetor[inicio]
        a = inicio + 1
        b = fim
        while True:
            while(a <= b and vetor[a] <= pivo):
                a += 1
            while(b >= a and vetor[b] >= pivo):
                b -= 1

            if a <= b:
                aux = vetor[a]
                vetor[a] = vetor[b]
                vetor[b] = aux
            else:
                vetor[inicio] = vetor[b]
                vetor[b] = pivo
                return b

    if inicio < fim:
        # print vetor
        divisor = partition(inicio, fim, vetor)
        quick_sort(inicio, divisor - 1, vetor)
        quick_sort(divisor + 1, fim, vetor)

def counting_sort(vetor, digito=None):
    tamanho_vetor = len(vetor)

    # Encontrando maior valor do vetor
    maior_valor = None
    for i in range(0, tamanho_vetor):
        # Necessário para o radix
        if digito is not None:
            valor_digito = vetor[i] // 10**digito % 10
            if valor_digito > maior_valor:
                maior_valor = valor_digito
        else:
            if vetor[i] > maior_valor:
                maior_valor = vetor[i]

    # Criando vetor auxiliar do tamanho do maior valor
    vetor_aux = [0 for i in range(0, maior_valor + 1)]

    # Frequência de valores
    for i in range(0, tamanho_vetor):
        # Necessário para o radix
        if digito is not None:
            valor_digito = vetor[i] // 10**digito % 10
            vetor_aux[valor_digito] = vetor_aux[valor_digito] + 1
        else:
            vetor_aux[vetor[i]] = vetor_aux[vetor[i]] + 1

    # Acumulando valores no vetor auxiliar
    for i in range(1, maior_valor + 1):
        vetor_aux[i] = vetor_aux[i] + vetor_aux[i - 1]

    # Atribuindo valor no vetor ordenado
    vetor_ordenado = [None for i in range(0, tamanho_vetor)]
    for i in range(tamanho_vetor - 1, -1, -1):
        # Necessário para o radix
        if digito is not None:
            valor_digito = vetor[i] // 10**digito % 10
            vetor_ordenado[vetor_aux[valor_digito] - 1] = vetor[i]
            vetor_aux[valor_digito] = vetor_aux[valor_digito] - 1
        else:
            vetor_ordenado[vetor_aux[vetor[i]] - 1] = vetor[i]
            vetor_aux[vetor[i]] = vetor_aux[vetor[i]] - 1

    return vetor_ordenado

def radix_sort(vetor):
    digitos = len(str(max(vetor)))

    for indice_digito in range(0, digitos):
        vetor = counting_sort(vetor, indice_digito)
        # for i in vetor:
        #     print indice_digito, i
    return vetor

def heap_sort(vetor):

    def esquerdo(i):
        return (2 * i) + 1

    def direito(i):
        return (2 * i) + 2

    def max_heapfy(vetor, tamanho_heap, i):
        e = esquerdo(i)
        d = direito(i)
        maior = i

        if e <= tamanho_heap and vetor[e] > vetor[i]:
            maior = e
        if d <= tamanho_heap and vetor[d] > vetor[maior]:
            maior = d

        if maior != i:
            aux = vetor[maior]
            vetor[maior] = vetor[i]
            vetor[i] = aux
            max_heapfy(vetor, tamanho_heap, maior)

    def build_max_heap(vetor, tamanho_heap):
        for i in range(tamanho_heap / 2, -1, -1):
            max_heapfy(vetor, tamanho_heap, i)

    # Construindo a heap máximo inicial
    tamanho_heap = len(vetor) - 1
    build_max_heap(vetor, tamanho_heap)

    for i in range(tamanho_heap, 0, -1):
        # Trocando elemento da raiz com último elemento da heap máximo
        aux = vetor[i]
        vetor[i] = vetor[0]
        vetor[0] = aux

        # Decrementando tamanho da heap
        tamanho_heap -= 1

        # Tornando o heap máximo novamente
        max_heapfy(vetor, tamanho_heap, 0)

    return vetor