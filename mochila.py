# -*- coding: utf-8 -*-

from datetime import datetime

def mochila(capacidade, qtd_itens, pesos, valores):

    def get_produtos_escolhidos(vetor_solucao, tabela, capacidade, qtd_itens, pesos):
        if qtd_itens:
            # Verificando se o valor veio da utilização do produto anterior ou do atual
            if tabela[capacidade][qtd_itens] == tabela[capacidade][qtd_itens - 1]:
                vetor_solucao[qtd_itens] = 0
            else:
                vetor_solucao[qtd_itens] = 1
                capacidade = capacidade - pesos[qtd_itens]

            get_produtos_escolhidos(
                vetor_solucao, 
                tabela, 
                capacidade, 
                qtd_itens - 1,
                pesos
            )

    # Construindo a tabela inicial
    tabela = list()
    for i in range(0, capacidade + 1):
        colunas = list()
        for j in range(0, qtd_itens + 1):
            colunas.append(0)
        tabela.append(colunas)

    # Populando a tabela utilizando a equação de recorrência
    for c in range(1, capacidade + 1):
        for i in range(1, qtd_itens + 1):
            anterior = tabela[c][i - 1]
            atual = tabela[c - pesos[i]][i - 1] + valores[i]
            if pesos[i] <= c and atual > anterior:
                tabela[c][i] = atual
            else:
                tabela[c][i] = anterior

    # Obtendo produtos escolhidos
    vetor_solucao = [0 for i in range(0, qtd_itens + 1)]
    get_produtos_escolhidos(vetor_solucao, tabela, capacidade, qtd_itens, pesos)

    # Imprimindo resultados
    print('\nPesos dos itens: {}'.format(pesos[1:]))
    print('Valores dos itens: {}'.format(valores[1:]))
    print('Capacidade da mochila: {}\n'.format(capacidade))
    print('Tabela solução:')
    
    i = 0
    for linha in tabela:
        print('{} - {}'.format(i, linha))
        i += 1

    print('\nMelhor benefício: {}'.format(tabela[capacidade][qtd_itens]))

    produtos_escolhidos = list()
    for i in range(1, qtd_itens + 1):
        if vetor_solucao[i]:
            produtos_escolhidos.append(i)
    print('Produtos escolhidos: {}'.format(produtos_escolhidos))
    

instancias = {
    1: 'mochila_teste.txt',
    2: 'mochila01.txt',
    3: 'mochila02.txt',
    4: 'mochila1000.txt',
    5: 'mochila2500.txt',
    6: 'mochila5000.txt',
}

# Iniciando
print('\n##### Atividade do problema da mochila inteira #####\n')

# Exibindo instâncias
print('Instâncias:\n')
for chave, valor in instancias.items():
    print('{} - {}'.format(chave, valor))

instancia = input('\nEscolha a instância desejada acima: ')

# Transformando o arquivo com a instância escolhida
capacidade = 0
qtd_itens = 0
pesos = [0]
valores = [0]
nome_arquivo = instancias.get(instancia)
with open('instancias_mochila/{}'.format(nome_arquivo)) as arquivo:
    primeira_linha = True
    for linha in arquivo.readlines():
        numeros = linha.split(' ')
        if primeira_linha:
            qtd_itens = int(numeros[0].strip())
            capacidade = int(numeros[1].strip())
            primeira_linha = False
        else:
            pesos.append(int(numeros[0].strip()))
            valores.append(int(numeros[1].strip()))
arquivo.close()

antes = datetime.now()
mochila(capacidade, qtd_itens, pesos, valores)
depois = datetime.now()

print('\nTempo de execução: {} segundos'.format((depois - antes).total_seconds()))

