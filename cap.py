# -*- coding: utf-8 -*-

import json
from datetime import datetime
from random import randint

ids_horarios = list()
ids_horarios_dia = list()

ids_salas = list()
capacidades_salas = list()
custos_salas = list()

ids_aulas = list()
ids_horarios_aulas = list()
ids_horarios_dia_aulas = list()
ids_diarios_aulas = list()
tamanhos_turmas_aulas = list()
qtd_aulas_diario = list()


def processando_instancia(horarios, salas, aulas):
    # Criando os vetores relacionados aos horários
    for id_horario in horarios:
        for d in range(1, 6):
            ids_horarios_dia.append('{}_{}'.format(id_horario, d))
        ids_horarios.append(id_horario)

    # Criando os vetores relacionados às salas ordenados pelos custos
    salas_a_ordenar = list()
    for id_sala, dados_sala in salas.items():
        dados_sala.update({'id': int(id_sala)})
        salas_a_ordenar.append(dados_sala)

    salas_ordenadas = sorted(salas_a_ordenar, key=lambda sala: sala.get('custo'))
    for dados_sala in salas_ordenadas:
        ids_salas.append(dados_sala.get('id'))
        capacidades_salas.append(dados_sala.get('capacidade'))
        custos_salas.append(dados_sala.get('custo'))

    # Criando os vetores relacionados às aulas ordenados pelos diários
    aulas_a_ordenar = list()
    for id_aula, dados_aula in aulas.items():
        dados_aula.update({'id': int(id_aula)})
        aulas_a_ordenar.append(dados_aula)

    aulas_ordenadas = sorted(aulas_a_ordenar, key=lambda aula: aula.get('id_diario'))
    for dados_aula in aulas_ordenadas:
        ids_aulas.append(dados_aula.get('id'))
        ids_horarios_aulas.append(dados_aula.get('id_horario_aula'))
        ids_horarios_dia_aulas.append('{}_{}'.format(
                dados_aula.get('id_horario_aula'),
                dados_aula.get('dia_semana'),
            )
        )
        ids_diarios_aulas.append(dados_aula.get('id_diario'))
        tamanhos_turmas_aulas.append(dados_aula.get('qtd_alunos'))
        qtd_aulas_diario.append(dados_aula.get('qtd_aulas_diario'))

def construcao():
    # Criando matriz solução (ids_horarios_dia x ids_salas)
    solucao = list()
    for i in ids_horarios_dia:
        slots_salas = list()
        for j in ids_salas:
            slots_salas.append(None)
        solucao.append(slots_salas)

    # Gerando solução inicial
    qtd_salas = len(ids_salas)
    qtd_aulas = len(ids_aulas)
    id_diario = None
    # Aulas ordenadas pelos diários
    for indice_aula in range(0, qtd_aulas):
        if ids_diarios_aulas[indice_aula] != id_diario:
            id_diario = ids_diarios_aulas[indice_aula]
        else:
            continue

        indice_horario_solucao = ids_horarios_dia.index(ids_horarios_dia_aulas[indice_aula])
        # Salas ordenadas pelos custos
        for indice_sala in range(0, qtd_salas):
            # Sala vazia no horário da aula
            if solucao[indice_horario_solucao][indice_sala] == None:
                # Capacidade da sala suporta turma da aula
                if tamanhos_turmas_aulas[indice_aula] <= capacidades_salas[indice_sala]:
                    # Aulas do mesmo diário na mesma sala
                    # Caso não tenha nenhuma aula alocada no horário da aula nesta sala (indice_sala)
                    # e a capacidade da sala suporte o tamanho da turma da aula, será verificado
                    # se os outros horários da sala estão vazios para alocar todas as aulas do diário.
                    # Caso não suporte, será testada a próxima sala.
                    sala_suporta_todas_aulas = True
                    for indice_aula_diario in range(indice_aula + 1, indice_aula + qtd_aulas_diario[indice_aula]):
                        indice_horario_solucao_diario = ids_horarios_dia.index(ids_horarios_dia_aulas[indice_aula_diario])
                        if solucao[indice_horario_solucao_diario][indice_sala] != None:
                            sala_suporta_todas_aulas = False
                            break

                    # Alocando todas as aulas do diário
                    if sala_suporta_todas_aulas:
                        for indice_aula_diario in range(indice_aula, indice_aula + qtd_aulas_diario[indice_aula]):
                            indice_horario_solucao_diario = ids_horarios_dia.index(ids_horarios_dia_aulas[indice_aula_diario])
                            solucao[indice_horario_solucao_diario][indice_sala] = indice_aula_diario
                        break
    return solucao

def realocacao(solucao, indice_sala_inicial):
    nova_solucao = [linha[:] for linha in solucao]
    qtd_salas = len(ids_salas)
    # Percorrendo solução
    for indice_horario_dia in range(0, len(ids_horarios_dia)):
        for indice_sala_solucao in range(indice_sala_inicial, qtd_salas):
            indice_aula = nova_solucao[indice_horario_dia][indice_sala_solucao]
            primeira_aula_diario = True
            if indice_aula > 0 and ids_diarios_aulas[indice_aula] == ids_diarios_aulas[indice_aula - 1]:
                primeira_aula_diario = False
            # Verificando se existe aula alocada e se ela é primeira aula do diário
            if indice_aula != None and primeira_aula_diario:
                # Buscando uma nova sala para a aula
                for indice_sala in range(0, qtd_salas):
                    # Se sala estiver vazia no horário da aula
                    if nova_solucao[indice_horario_dia][indice_sala] == None:
                        # Capacidade da sala suporta turma da aula
                        if tamanhos_turmas_aulas[indice_aula] <= capacidades_salas[indice_sala]:
                            # Aulas do mesmo diário na mesma sala
                            sala_suporta_todas_aulas = True
                            for indice_aula_diario in range(indice_aula + 1, indice_aula + qtd_aulas_diario[indice_aula]):
                                indice_horario_solucao_diario = ids_horarios_dia.index(ids_horarios_dia_aulas[indice_aula_diario])
                                if nova_solucao[indice_horario_solucao_diario][indice_sala] != None:
                                    sala_suporta_todas_aulas = False
                                    break

                            # Alocando todas as aulas do diário na nova sala vazia
                            if sala_suporta_todas_aulas:
                                for indice_aula_diario in range(indice_aula, indice_aula + qtd_aulas_diario[indice_aula]):
                                    indice_horario_solucao_diario = ids_horarios_dia.index(ids_horarios_dia_aulas[indice_aula_diario])
                                    nova_solucao[indice_horario_solucao_diario][indice_sala] = indice_aula_diario
                                    nova_solucao[indice_horario_solucao_diario][indice_sala_solucao] = None
                                break
    return nova_solucao

def troca(solucao, indice_sala_inicial):
    nova_solucao = [linha[:] for linha in solucao]
    qtd_salas = len(ids_salas)
    # Percorrendo solução
    for indice_horario_dia in range(0, len(ids_horarios_dia)):
        for indice_sala_solucao in range(indice_sala_inicial, qtd_salas):
            indice_aula = nova_solucao[indice_horario_dia][indice_sala_solucao]
            primeira_aula_diario = True
            if indice_aula > 0 and ids_diarios_aulas[indice_aula] == ids_diarios_aulas[indice_aula - 1]:
                primeira_aula_diario = False
            # Verificando se existe aula alocada e se ela é primeira aula do diário
            if indice_aula != None and primeira_aula_diario:
                # Buscando uma nova sala para a aula
                for indice_sala in range(0, qtd_salas):
                    # Capacidade da sala suporta turma da aula
                    if tamanhos_turmas_aulas[indice_aula] <= capacidades_salas[indice_sala]:
                        # Aulas do mesmo diário na mesma sala
                        salas_suportam_troca = True
                        for indice_aula_diario in range(indice_aula, indice_aula + qtd_aulas_diario[indice_aula]):
                            indice_horario_solucao_diario = ids_horarios_dia.index(ids_horarios_dia_aulas[indice_aula_diario])
                            indice_aula_troca = nova_solucao[indice_horario_solucao_diario][indice_sala]
                            if indice_aula_troca != None:
                                # Removendo aula da troca caso o tamanho das turma não seja suportada pelas salas a ser trocada
                                if tamanhos_turmas_aulas[indice_aula_troca] > capacidades_salas[indice_sala_solucao]:
                                    salas_suportam_troca = False
                                    break

                                # Verificando se todos os horários do diário a ser trocado
                                # correpondem aos horários do diário que se deseja trocar
                                id_diario_anterior = ids_diarios_aulas[indice_aula_troca]
                                indice_aula_troca_inicial = indice_aula_troca
                                # Encontrando índice inicial do diário a ser trocado
                                while indice_aula_troca_inicial >= 0 and id_diario_anterior == ids_diarios_aulas[indice_aula_troca_inicial]:
                                    id_diario_anterior = ids_diarios_aulas[indice_aula_troca_inicial]
                                    indice_aula_troca_inicial -= 1
                                indice_aula_troca_inicial += 1

                                # Percorrendo todos os horários do diários a ser troca
                                for indice_aula_trocado in range(indice_aula_troca_inicial, indice_aula_troca_inicial + qtd_aulas_diario[indice_aula_troca_inicial]):
                                    horario_trocado = ids_horarios_dia_aulas[indice_aula_trocado]
                                    existe_horario = False
                                    for indice_aula_deseja_trocar in range(indice_aula, indice_aula + qtd_aulas_diario[indice_aula]):
                                        horario_deseja_trocar = ids_horarios_dia_aulas[indice_aula_deseja_trocar]
                                        if horario_trocado == horario_deseja_trocar:
                                            existe_horario = True

                                    if not existe_horario:
                                        salas_suportam_troca = False
                                        break

                        # Alocando todas as aulas do diário na nova sala vazia
                        if salas_suportam_troca:
                            for indice_aula_diario in range(indice_aula, indice_aula + qtd_aulas_diario[indice_aula]):
                                indice_horario_solucao_diario = ids_horarios_dia.index(ids_horarios_dia_aulas[indice_aula_diario])
                                indice_aula_troca = nova_solucao[indice_horario_solucao_diario][indice_sala]
                                nova_solucao[indice_horario_solucao_diario][indice_sala] = indice_aula_diario
                                nova_solucao[indice_horario_solucao_diario][indice_sala_solucao] = indice_aula_troca
                            break
    return nova_solucao

def avaliacao(solucao, acao=None):
    custo = 0
    qtd_salas = len(ids_salas)
    for indice_horario_dia in range(0, len(ids_horarios_dia)):
        for indice_sala in range(0, qtd_salas):
            if solucao[indice_horario_dia][indice_sala] != None:
                custo += custos_salas[indice_sala]
    if acao:
        print(u'Custo da {}: {}'.format(acao, custo))
    return custo

def vnd(solucao_inicial=None, indice_sala_inicial=70):
    solucao = None
    if solucao_inicial:
        # VNS
        solucao = solucao_inicial
    else:
        # VND
        solucao = construcao()

    custo_anterior = avaliacao(solucao, u'solução inicial')
    custo = 0
    while custo < custo_anterior:
        # Para escapar do custo: 0
        if custo > 0:
            custo_anterior = custo
            solucao = nova_solucao

        # Melhoria por realocacao
        nova_solucao = realocacao(solucao, indice_sala_inicial)
        custo = avaliacao(nova_solucao, u'realocação 1')
        while(custo < custo_anterior):
            custo_anterior = custo
            solucao = nova_solucao
            nova_solucao = realocacao(solucao, indice_sala_inicial)
            custo = avaliacao(nova_solucao, u'realocação 2')

        # Melhoria por troca
        nova_solucao = troca(solucao, indice_sala_inicial)
        custo = avaliacao(nova_solucao, u'troca 1')
        while(custo < custo_anterior):
            custo_anterior = custo
            solucao = nova_solucao
            nova_solucao = troca(solucao, indice_sala_inicial)
            custo = avaliacao(nova_solucao, u'troca 2')

        # Verificando se melhora novamente com a realocação
        nova_solucao = realocacao(solucao, indice_sala_inicial)
        custo = avaliacao(nova_solucao, u'realocação 3')

    if not solucao_inicial:
        print(u'\nCusto da solução após VND: {}\n'.format(custo_anterior))
    return solucao

def pertubacao(solucao, indice_inicial_sala):
    nova_solucao = [linha[:] for linha in solucao]
    qtd_salas = len(ids_salas)
    # Percorrendo solução
    for indice_horario_dia in range(0, len(ids_horarios_dia)):
        for indice_sala_solucao in range(indice_inicial_sala, qtd_salas):
            indice_aula = nova_solucao[indice_horario_dia][indice_sala_solucao]
            primeira_aula_diario = True
            if indice_aula > 0 and ids_diarios_aulas[indice_aula] == ids_diarios_aulas[indice_aula - 1]:
                primeira_aula_diario = False
            # Verificando se existe aula alocada e se ela é primeira aula do diário
            if indice_aula != None and primeira_aula_diario:
                # Escolhendo aleotariamente uma sala para trocar a aula
                indice_sala = randint(0, qtd_salas - 1)
                # Capacidade da sala suporta turma da aula
                if tamanhos_turmas_aulas[indice_aula] <= capacidades_salas[indice_sala]:
                    # Aulas do mesmo diário na mesma sala
                    salas_suportam_troca = True
                    for indice_aula_diario in range(indice_aula,
                                                    indice_aula + qtd_aulas_diario[indice_aula]):
                        indice_horario_solucao_diario = ids_horarios_dia.index(
                            ids_horarios_dia_aulas[indice_aula_diario]
                        )
                        indice_aula_troca = nova_solucao[indice_horario_solucao_diario][indice_sala]
                        if indice_aula_troca != None:
                            # Removendo aula da troca caso o tamanho das turma não seja suportada pelas salas a ser trocada
                            if tamanhos_turmas_aulas[indice_aula_troca] > capacidades_salas[indice_sala_solucao]:
                                salas_suportam_troca = False
                                break

                            # Verificando se todos os horários do diário a ser trocado
                            # correpondem aos horários do diário que se deseja trocar
                            id_diario_anterior = ids_diarios_aulas[indice_aula_troca]
                            indice_aula_troca_inicial = indice_aula_troca
                            # Encontrando índice inicial do diário a ser trocado
                            while indice_aula_troca_inicial >= 0 and id_diario_anterior == ids_diarios_aulas[indice_aula_troca_inicial]:
                                id_diario_anterior = ids_diarios_aulas[indice_aula_troca_inicial]
                                indice_aula_troca_inicial -= 1
                            indice_aula_troca_inicial += 1

                            # Percorrendo todos os horários do diários a ser troca
                            for indice_aula_trocado in range(indice_aula_troca_inicial, indice_aula_troca_inicial + qtd_aulas_diario[indice_aula_troca_inicial]):
                                horario_trocado = ids_horarios_dia_aulas[indice_aula_trocado]
                                existe_horario = False
                                for indice_aula_deseja_trocar in range(indice_aula, indice_aula + qtd_aulas_diario[indice_aula]):
                                    horario_deseja_trocar = ids_horarios_dia_aulas[indice_aula_deseja_trocar]
                                    if horario_trocado == horario_deseja_trocar:
                                        existe_horario = True

                                if not existe_horario:
                                    salas_suportam_troca = False
                                    break

                    # Alocando todas as aulas do diário na nova sala vazia
                    if salas_suportam_troca:
                        for indice_aula_diario in range(indice_aula, indice_aula + qtd_aulas_diario[indice_aula]):
                            indice_horario_solucao_diario = ids_horarios_dia.index(ids_horarios_dia_aulas[indice_aula_diario])
                            indice_aula_troca = nova_solucao[indice_horario_solucao_diario][indice_sala]
                            nova_solucao[indice_horario_solucao_diario][indice_sala] = indice_aula_diario
                            nova_solucao[indice_horario_solucao_diario][indice_sala_solucao] = indice_aula_troca
    return nova_solucao

def vns(iteracoes, indice_inicial_sala=30):
    k = 1
    solucao = construcao()
    while k <= iteracoes:
        print('\n***** VND na solução pertubada:')
        solucao_pertubada = pertubacao(solucao, indice_inicial_sala)
        solucao_minimo_local = vnd(solucao_pertubada)
        print('***** Custo mínimo local: {}'.format(avaliacao(solucao_minimo_local)))
        if avaliacao(solucao_minimo_local) < avaliacao(solucao):
            solucao = solucao_minimo_local
        else:
            k += 1
        custo_solucao = avaliacao(solucao)
        print('***** Custo da solução atual: {}\n'.format(custo_solucao))

    print(u'\nCusto da solução após VNS: {}\n'.format(custo_solucao))
    return solucao

def verificar_aula_nao_alocada(solucao):
    print(u'\nVerificando se todas aulas foram alocadas em salas ...')
    qtd_salas = len(ids_salas)
    qtd_aulas = len(ids_aulas)
    indice_aulas_nao_alocadas = list()
    for indice_aula in range(0, qtd_aulas):
        sala = None
        for indice_horario_dia in range(0, len(ids_horarios_dia)):
            for indice_sala in range(0, qtd_salas):
                indice_aula_solucao = solucao[indice_horario_dia][indice_sala]
                if indice_aula == indice_aula_solucao:
                    sala = indice_sala
        if sala == None:
            indice_aulas_nao_alocadas.append(indice_aula)

    for indice_aula in indice_aulas_nao_alocadas:
        id_aula = ids_aulas[indice_aula]
        id_diario = ids_diarios_aulas[indice_aula]
        horario_aula = ids_horarios_dia_aulas[indice_aula]
        tamanho_turma = tamanhos_turmas_aulas[indice_aula]
        print(u'\nA aula {} do diário {} que possui o horário {} e tamanho da turma {} não foi '
              u'alocada em nenhuma sala.'.format(id_aula, id_diario, horario_aula, tamanho_turma))

        # print(u'Salas não utilizadas no horário da aula que suportam o tamanho da turma:')
        # indice_horario = ids_horarios_dia.index(horario_aula)
        # for indice_sala in range(0, qtd_salas):
        #     if solucao[indice_horario][indice_sala] == None:
        #         capacidade_sala = capacidades_salas[indice_sala]
        #         if tamanho_turma <= capacidade_sala:
        #             id_sala = ids_salas[indice_sala]
        #             print(u' - Sala {} de capacidade {}.'.format(id_sala, capacidade_sala))

    if not indice_aulas_nao_alocadas:
        print(u'Todas as alocadas em salas.')

def salvar_em_arquivo_solucao(solucao):
    qtd_salas = len(ids_salas)
    qtd_aulas = len(ids_aulas)
    arquivo_solucao = open('aulas_salas.txt', 'w')
    arquivo_solucao.writelines('id_diario;id_aula;id_sala\n\n')
    id_diario_anterior = None
    indice_sala_anterior = None
    for indice_aula in range(0, qtd_aulas):
        id_diario = ids_diarios_aulas[indice_aula]
        for indice_horario_dia in range(0, len(ids_horarios_dia)):
            for indice_sala in range(0, qtd_salas):
                indice_aula_solucao = solucao[indice_horario_dia][indice_sala]
                if indice_aula == indice_aula_solucao:
                    if id_diario == id_diario_anterior and indice_sala_anterior != indice_sala:
                        print id_diario
                    arquivo_solucao.writelines('{};{};{}\n'.format(
                        ids_diarios_aulas[indice_aula],
                        ids_aulas[indice_aula],
                        ids_salas[indice_sala]
                    ))
                    indice_sala_anterior = indice_sala
                    break
                    break
        id_diario_anterior = id_diario
    arquivo_solucao.close()

def main():
    # Iniciando
    print('\n##### Problema de alocação de aulas a sala - Instância: IFPB/JP #####\n')

    # Lendo instância
    horarios = json.loads(open('instancias_cap/horarios.json').read())
    salas = json.loads(open('instancias_cap/salas.json').read())
    aulas = json.loads(open('instancias_cap/aulas.json').read())
    processando_instancia(horarios, salas, aulas)

    print('\nAlgoritmos:\n')
    print('{} - {}'.format(1, 'VND'))
    print('{} - {}'.format(2, 'VNS'))
    metodo = input('\nEscolha o algoritmo: ')

    solucao = None
    antes = datetime.now()
    if metodo == 1:
        solucao = vnd()
    elif metodo == 2:
        iteracoes = input('\nQuantidade de iterações: ')
        solucao = vns(iteracoes=iteracoes)
    depois = datetime.now()
    print('Tempo de execução: {} segundos'.format((depois - antes).total_seconds()))

    verificar_aula_nao_alocada(solucao)
    salvar_em_arquivo_solucao(solucao)

if __name__ == "__main__":
    main()

