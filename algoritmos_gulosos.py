# -*- coding: utf-8 -*-

from algoritmos_ordenacao import *

# Funções acessórias
def get_key(dict, value):
     return [key for key in dict.keys() if (dict[key] == value)][0]

def mapeamento_arestas(matriz):
    arestas = dict()
    i = 1
    for linha in matriz:
        j = i + 1
        for valor_aresta in linha[i:]:
            if valor_aresta:
                arestas.update({(i, j): valor_aresta})
            j += 1
        i += 1
    return arestas

def mapeamento_arestas_completo(matriz):
    arestas = dict()
    i = 1
    for linha in matriz:
        j = 1
        for valor_aresta in linha:
            if valor_aresta:
                arestas.update({(i, j): valor_aresta})
            j += 1
        i += 1
    return arestas

# Heap mínimo
def esquerdo(i):
    return (2 * i) + 1

def direito(i):
    return (2 * i) + 2

def pai(i):
    return i / 2

def min_heapfy(vetor, tamanho_heap, i):
    e = esquerdo(i)
    d = direito(i)
    menor = i

    if e < tamanho_heap and vetor[e] < vetor[i]:
        menor = e
    if d < tamanho_heap and vetor[d] < vetor[menor]:
        menor = d

    if menor != i:
        aux = vetor[menor]
        vetor[menor] = vetor[i]
        vetor[i] = aux
        min_heapfy(vetor, tamanho_heap, menor)

def build_min_heap(vetor, tamanho_heap):
    for i in range((tamanho_heap / 2) - 1, -1, -1):
        min_heapfy(vetor, tamanho_heap, i)
    return vetor

# Fila de prioridade
def extract_min(heap, tamanho_heap):
    if tamanho_heap < 1:
        return

    min = heap[0]
    heap[0] = heap[tamanho_heap - 1]
    tamanho_heap -= 1
    heap = heap[:-1]
    min_heapfy(heap, tamanho_heap - 1, 0)
    return min, heap

def decrease_key(heap_minima, i, chave):
    if chave > heap_minima[i]:
        return 

    heap_minima[i] = chave;
    while i > 0 and heap_minima[pai(i)] > heap_minima[i]:
        aux = heap_minima[i]
        heap_minima[i] = heap_minima[pai(i)]
        heap_minima[pai(i)] = aux
        i = pai(i)

# Grafo
def lista_adjacencia(arestas, elemento):
    adjacentes = list()
    for aresta in arestas:
        if aresta[0] == elemento:
            adjacentes.append(aresta[1])
        elif aresta[1] == elemento:
            adjacentes.append(aresta[0])
    return set(adjacentes)

# Kruskal
def kruskal(numero_vertices, matriz):

    def make_set(conjunto_relacoes, pesos, vertice):
        conjunto_relacoes.update({vertice: vertice})
        pesos.update({vertice: 0})

    def find_set(conjunto_relacoes, vertice):
        vertice_relacionado = conjunto_relacoes.get(vertice)
        if vertice_relacionado != vertice:
            vertice_relacionado = find_set(conjunto_relacoes, vertice_relacionado)
        return vertice_relacionado

    def union(conjunto_relacoes, pesos, u, v):
        def atualizar_arvore(vertice, relacao):
            if vertice != relacao:
                conjunto_relacoes.update({vertice: relacao})
                atualizar_arvore(conjunto_relacoes.get(vertice), relacao)

        u_peso = pesos.get(u)
        v_peso =  pesos.get(v)
        u_relacao = find_set(conjunto_relacoes, u)
        v_relacao = find_set(conjunto_relacoes, v)

        if u_peso < v_peso:
            atualizar_arvore(conjunto_relacoes.get(u), v_relacao)
            conjunto_relacoes.update({u: v_relacao})
        else:
            atualizar_arvore(conjunto_relacoes.get(v), u_relacao)
            conjunto_relacoes.update({v: u_relacao})
        
        if u_peso == v_peso:
            pesos.update({v: v_peso + 1})

    # Início
    arestas = mapeamento_arestas_completo(matriz)
    arestas_solucao = dict()
    conjunto_relacoes = dict()
    pesos = dict()

    # Inicializando conjunto_relacoes e pesos para cada vértice
    for v in range(1, numero_vertices + 1):
        make_set(conjunto_relacoes, pesos, v)

    # Ordenando valores das arestas
    valores_arestas_ordenados = heap_sort(arestas.values())

    # Percorrendo arestas em ordem crescente
    for valor_aresta in valores_arestas_ordenados:
        # Obtendo aresta através do valor dela. Pode ser feito por ser arbritrária a escolha
        # quando arestas empatam
        aresta = get_key(arestas, valor_aresta)

        # Removendo aresta da lista de arestas para o funcionamento correto da get_key, pois 
        # já aresta será processada
        del arestas[aresta]

        # Verificando se vértices da aresta estão na mesma árvore para não formar ciclo
        # Caso não estejam a aresta é adicionada a solução e é feita união dos vértices
        u = aresta[0]
        v = aresta[1]
        if find_set(conjunto_relacoes, u) != find_set(conjunto_relacoes, v):
            arestas_solucao.update({aresta: valor_aresta})
            union(conjunto_relacoes, pesos, u, v)

    # Exibindo resultado
    resultado = 0
    for valor_aresta in arestas_solucao.values():
        resultado += valor_aresta

    print(u'\nResultado da MST: {}\n'.format(resultado))
    print(u'Arestas que formaram a MST: {}'.format(arestas_solucao))

# PRIM
def prim(numero_vertices, matriz, vertice_raiz=1):
    # Variáveis
    chave = dict()
    pai = dict()
    vertices_processados = list()
    arestas = mapeamento_arestas_completo(matriz)

    # Inicializando chave e pai para cada vértice
    for v in range(1, numero_vertices + 1):
        chave.update({v: float("inf")})
        pai.update({v: None})
    chave.update({vertice_raiz: 0})

    # Construindo a heap mínima
    heap_minima = build_min_heap(chave.values(), len(chave))

    while(heap_minima):
        # Extraindo o menor elemento da heap_minima
        menor_chave, heap_minima = extract_min(heap_minima, len(heap_minima))
        # Obtendo o vertice que possui a menor chave
        u = get_key(chave, menor_chave)
        # Adicionando o vertice u nos vertices_processados para não reprocessado
        vertices_processados.append(u)
        # Percorrendo os vizinhos de u
        for v in lista_adjacencia(arestas, u):
            # Obtendo o valor da aresta(u, v)
            valor_aresta = arestas.get((u, v))
            # Se o vertice não foi processado e o valor da aresta é menor que a chave de v
            if v not in vertices_processados and valor_aresta < chave.get(v):
                # Atualizar o pai de v com u
                pai.update({v: u})
                # Atualizando a heap_minima com o novo valor de v(valor_aresta) 
                decrease_key(heap_minima, heap_minima.index(chave.get(v)), valor_aresta)
                # Atualizando a chave de v com o valor da aresta
                chave.update({v: valor_aresta})

    # Resultado das arestas escolhidas
    arestas_escolhidas = list()
    for vertice, pai in pai.items():
        if pai and vertice:
            arestas_escolhidas.append((pai, vertice))

    # Resultado
    resultado = 0
    for c in chave.values():
        if c:
            resultado += c 

    print(u'\nResultado da MST: {}\n'.format(resultado))
    print(u'Arestas que formaram a MST: {}'.format(arestas_escolhidas))

# Dijkstra
def dijkstra(numero_vertices, matriz, vertice_raiz=1):
    def relax(u, v, valor_aresta, chave, pai, heap_minima):
        if chave.get(v) > chave.get(u) + valor_aresta:
            nova_chave = chave.get(u) + valor_aresta
            decrease_key(heap_minima, heap_minima.index(chave.get(v)), nova_chave)
            chave.update({v: nova_chave})
            pai.update({v: u})

    # Variáveis
    chave = dict()
    pai = dict()
    vertices_visitados = list()
    arestas = mapeamento_arestas_completo(matriz)

    # Inicializando chave e pai para cada vértice
    for v in range(1, numero_vertices + 1):
        chave.update({v: float("inf")})
        pai.update({v: None})
    chave.update({vertice_raiz: 0})
    
    # Construindo a heap mínima
    heap_minima = build_min_heap(chave.values(), len(chave))
    
    while(heap_minima):
        # Extraindo o menor elemento da heap_minima
        menor_chave, heap_minima = extract_min(heap_minima, len(heap_minima))
        # Obtendo o vertice que possui a menor chave
        u = get_key(chave, menor_chave)
        # Adicionando o vertice u nos vertices_processados para não reprocessado
        vertices_visitados.append(u)
        # Percorrendo os vizinhos de u
        for v in lista_adjacencia(arestas, u):
            if not v in vertices_visitados:
                valor_aresta = arestas.get((u, v))
                relax(u, v, valor_aresta, chave, pai, heap_minima)
            
    print(u'\nResultado do caminho mínimo: {}'.format(chave.get(len(chave))))
